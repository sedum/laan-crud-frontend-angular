import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(private httpClient: HttpClient) { }

  users:any;
  authData = {email: "test@example.com", password: "testtest"};

  getData(){
    return this.httpClient.get('http://127.0.0.1:8000/api/users');
  }

  getUsersData() {
    this.getData().subscribe(res =>{
        console.log(res);
        this.users = res;
      })
  }

  ngOnInit(): void {

    this.httpClient.post<any>('http://127.0.0.1:8000/api/login', this.authData)
      .subscribe(res => {
        console.log(res);
        this.getUsersData();
    }
      , error => console.log(error))

  }

}
